#!/usr/bin/env bash
sudo firewall-cmd \
  --permanent \
  --zone=public\
  --new-service=container_qbittorrent \
  --add-port=QBITTORRENT-PORT/tcp
sudo firewall-cmd --reload
