#!/usr/bin/env bash
sudo firewall-cmd \
  --permanent \
  --zone=public \
  --new-service=container_woodpecker \
  --add-port=WOODPECKER-PORT/tcp
sudo firewall-cmd --reload
