#!/usr/bin/env bash
sudo firewall-cmd \
  --permanent \
  --zone=public \
  --new-service=container_navidrome \
  --add-port=4533/tcp
sudo firewall-cmd --reload
