#!/usr/bin/env bash
sudo firewall-cmd \
  --permanent \
  --zone=public \
  --new-service=container_portainer \
  --add-port=PORTAINER-PORT/tcp
sudo firewall-cmd --reload
