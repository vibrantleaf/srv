#!/usr/bin/env bash
# vaultwarden port(s)
sudo firewall-cmd \
  --permanent \
  --zone=public \
  --new-service=container_vaultwarden \
  --add-port=8000/tcp \
  --add-port=8000/udp
sudo firewall-cmd --reload