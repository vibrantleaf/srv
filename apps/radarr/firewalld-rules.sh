#!/usr/bin/env bash
sudo firewall-cmd \
  --permanent \
  --zone=public \
  --new-service=container_radarr \
  --add-port=7878/tcp
sudo firewall-cmd --reload
