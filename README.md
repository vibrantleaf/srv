# Srv
my IaC (Infrastructure as Code) & setup for my homeserver + some custom tooling

Using:
- ⛰️ Rocky Linux [*link*](https://rockylinux.org/) - Community Enterprise Linux.
- ✈️ Cockpit [*link*](https://cockpit-project.org/) - Server Management.
- 🥫 LibVirt [*link*](https://libvirt.org/) - Virtualization Stack.
- ~~🐋 Docker [*link*](https://www.docker.com/) - OCI Container Engine.~~ *(NO LONGER IN USE.)*
- 🦭 Podman [*link*](https://podman.io/) - OCI Container Engine.
- 🔼 K3s [*link*](https://k3s.io/) - OCI Container Orchestration.
- 🌊 Crun [*link*](https://github.com/containers/crun) - OCI Container Runtime.
- 🦦 Linuxserver [*link*](https://www.linuxserver.io/) - Custom Community Made & Managed OCI Images.
- 🇬 Google DNS [*link*](https://developers.google.com/speed/public-dns) - DNS For OCI Containers.
- 🐙 Kompose [*link*](https://kompose.io/) - podman/docker `compose.yaml` to kubernetes `deploment.yaml` formated `.yaml` Converter. 
### setup:
#### step 1: Install Rocky Linux.
see: https://rockylinux.org/
#### step 2: Clone this repo.
```bash
git clone --remote-submodules --recurse-submodules https://codeberg.org/vibrantleaf/srv.git ~/srv
cd ~/srv
git pull origin main
git submodule update --recursive --remote --init
```
#### updating the repo
```bash
cd ~/srv
git stash
git pull origin main
git submodule update --recursive --remote --init
git stash apply --index
```